/*=================================================================
	File created by Yohann NICOLAS.

	New runeword Interface

=================================================================*/

#pragma once

void STDCALL printRunewordsPage();
DWORD STDCALL mouseRunewordsPageLeftDown(sWinMessage* msg);
DWORD STDCALL mouseRunewordsPageLeftUp(sWinMessage* msg);
