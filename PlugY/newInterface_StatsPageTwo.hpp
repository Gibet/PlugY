/*=================================================================
	File created by Yohann NICOLAS.

	New Stat Interface Page 2

=================================================================*/

#pragma once

void STDCALL printNewStatsPageTwo(int currentPage);
DWORD STDCALL mouseNewStatsPageTwoLeftDown(sWinMessage* msg);
DWORD STDCALL mouseNewStatsPageTwoLeftUp(sWinMessage* msg);
