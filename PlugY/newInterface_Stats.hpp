/*=================================================================
	File created by Yohann NICOLAS.

	New Stat Interface

=================================================================*/

#pragma once

void STDCALL printNewStatsPage();
DWORD STDCALL mouseNewStatsPageLeftDown(sWinMessage* msg);
DWORD STDCALL mouseNewStatsPageLeftUp(sWinMessage* msg);
